+-----------------------------------------------------------------------------------+
|      Astronomical Research Cameras, Inc. ARC Application Interface ( API )        |
+-----------------------------------------------------------------------------------+

To compile 32-bit libraries:

	make -f Makefile_x32 cpplibs


To compile 64-bit libraries:

	make -f Makefile_x64 cpplibs


To compile both 32-bit and 64-bit libraries:

	make -f Makefile cpplibs


The entire directory is a project that can be imported and compiled using eclipse.

