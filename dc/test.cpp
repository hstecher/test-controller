#include <iostream>
#include <cstdlib>
#include "CArcDevice.h"
#include "CArcPCIe.h"
#include "ArcDefs.h"
#include "CArcFitsFile.h"
#include "CArcDeinterlace.h"

using namespace std;
using namespace arc;
using namespace arc::device;
using namespace arc::fits;
using namespace arc::deinterlace;

/*
 * 	DC	'BVS',SET_VDETCOM_S
 * 	DC	'BVM',SET_VDETCOM_M
 *	DC	'BVD',SET_VDETCOM_D
 */
#define BVS 0x00425653
#define BVM 0x0042564d
#define BVD 0x00425644

const char lod_file[] = "/home/hstecher/controller_test/dc/DSP/bias.lod";
const int nrows = 1024;
const int ncols = 1024;
const int datasize = 2; // 16 bits
constexpr int bufSize = nrows * ncols * datasize;


// Expose Callback interface
// class CExposeListener : public CExpIFace
// {
  // void ExposeCallback(float elapsedTime)
  // {
    // cout << "Expose Callback - ellapsed time: " << elapsedTime << '\n';
  // }

  // void ReadCallback(int pixelCount)
  // {
    // cout << "Read Callback - pixel count: " << pixelCount << '\n';
  // }
// };

int main(int argc, char **argv)
{

  // CExposeListener cExposeListener;

  bool deinterlace = ((argc > 1) && (string(argv[1]) == "-d"));
  CArcPCIe::FindDevices();

  if(CArcPCIe::DeviceCount() > 0)
  {
    cout << "Found " << CArcPCIe::DeviceCount() << " device(s)" << '\n';

    CArcDevice *pArcDev = new CArcPCIe();

    pArcDev->Open(0);
    if(pArcDev->IsOpen())
    {
      cout << "Device opened sucessfully" << '\n';
    }
    else
    {
      cerr << "Device failed to open!. This almost certainly won't work" << '\n';
      exit(1);
    }


    cout << "Device Found: " << pArcDev->ToString() << '\n';
    cout << "Device Status: " << pArcDev->GetStatus() << '\n';

    if(pArcDev->IsControllerConnected())
    {
      cout << "Yay! Controller is connected." << '\n';
    }
    else
    {
      cout << "Uh-oh! Controller is NOT connected." << '\n';
      cout << "Check power. Check fiber." << '\n';
    }


    // Reset Controller in case it was left in a bad state
    cout << "Resetting Controller..." << flush;
    pArcDev->ResetController();
    cout << " OK." << '\n';

    // Return value variable
    int dRetVal;

    // Send  a bunch of Test Data Link Commands
    cout << "Multiple TDL test starting..." << flush;
    int good_tdl = 0;
    int bad_tdl = 0;
    for(int i=0; i<50000; i++)
    {
      int r = rand() % 65535;
      int dRetVal = pArcDev->Command(TIM_ID, TDL, r);
      if(dRetVal == r)
      {
        good_tdl++;
      }
      else
      {
        bad_tdl++;
        cout << "...  TDL " << i << " Failed! - got: " << dRetVal << '\n';
      }
    }
    cout << "... Got " << good_tdl << " good TDL results and " << bad_tdl << " bad TDL results" << '\n';
    // Load the timing board DSP code
    cout << "Uploading Timing Board Run-time DSP code... " << flush;
    pArcDev->LoadControllerFile(lod_file);

    // Setting up the controller
//    cout << "Setting up the controller\n";
//    pArcDev->SetupController( true, // reset controller
//			      true, // test data links
//			      false, // Don't power on
//			      nrows,   // row size
//			      ncols,   // col size

    cout << "done" << '\n';
    cout << "Device Status: " << pArcDev->GetStatus() << '\n';

    // Send  a bunch of Test Data Link Commands
    cout << "Multiple TDL test starting..." << flush;
    good_tdl=0;
    bad_tdl=0;
    for(int i=0; i<50000; i++)
    {
      int r = rand() % 65535;
      int dRetVal = pArcDev->Command(TIM_ID, TDL, r);
      if(dRetVal == r)
      {
        good_tdl++;
      }
      else
      {
        bad_tdl++;
        cout << "...  TDL " << i << " Failed! - got: " << dRetVal << '\n';
      }
    }
    cout << "... Got " << good_tdl << " good TDL results and " << bad_tdl << " bad TDL results" << '\n';


    // Try and do my VER command
    cout << "Attempting VER command... " << flush;
    dRetVal = pArcDev->Command(TIM_ID, 0x00564552);
    cout << "got software Version: " << dRetVal << '\n';
    
    cout << "Setting " << nrows << 'x' << ncols << "image size... " << flush;
    pArcDev->SetImageSize(nrows, ncols);
    cout << " OK" << '\n';

    cout << "Mapping Common Buffer... " << flush;

    pArcDev->MapCommonBuffer(bufSize);
    if(pArcDev->CommonBufferSize() == bufSize)
    {
      cout << "Common Buffer Mapped OK" << '\n';
    }
    else
    {
      cout << "Failed to Map Common Buffer" << '\n';
    }

    cout << "Setting Synthetic Image Mode... " << flush;
    pArcDev->SetSyntheticImageMode(true);
    if(pArcDev->IsSyntheticImageMode())
    {
      cout << "Synthetic Image Mode set" << '\n';
    }
    else
    {
      cout << "Synthetic Image Mode NOT set" << '\n';
      exit(1);
    }
 
    cout << "Quote-Unquote Exposing... " << flush;
    pArcDev->Expose(0.0, nrows, ncols, NULL, NULL, false);
    cout << "Seemed to expose OK" << '\n';

    // See what's in the buffer
    unsigned short *buf = (unsigned short *)pArcDev->CommonBufferVA();
    /*
    for (int i=0; i<10; i++)
    {
      cout << "Buffer[" << i << "]= " << buf[i] << '\n';
    }
    */

    if (deinterlace) {
      cout << "Deinterlacing\n";
      CArcDeinterlace cDlacer;

      cDlacer.RunAlg( buf, nrows, ncols, CArcDeinterlace::DEINTERLACE_IR_QUAD);
    }

    // Write FITS file
    cout << "Writing FITS file... " << flush;
    CArcFitsFile cFits("test.fits", nrows, ncols);
    cFits.Write(buf);
    cout << "OK." << '\n';


    pArcDev->Close();
  }
  else
  {
    cout << "No Devices Found" << '\n';
  }

  return 0;
}
