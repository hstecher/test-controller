#include <iostream>
#include <unordered_map>
#include <string>
#include <unistd.h>
#include "libgnirs.h"
#include <CExpIFace.h>








struct Config {
	std::string label;
	std::string lod_file;
	int nrows;
	int ncols;
	float exposure;
};

static const auto READ_TIMEOUT = 200;
const int SDSU3_PON_BIT = 0x400000;

std::unordered_map<std::string, Config> setups{
//	{"ffvf", {"Very Faint Object - Full frame", "./DSP/VeryFaintFullFrame.lod", 32768, 3072 * 4}},
//
//

	{"local-ffvb", {"Very Bright Object - Full frame", "./DSP/VeryBrightFullFrame.lod", 1024, 2048}},
	{"local-ffb",  {"Bright Object - Full frame", "./DSP/BrightFullFrame.lod", 1024, 12288}},
	{"local-fff",  {"Faint Object - Full frame", "./DSP/VeryFaintFullFrame.lod", 16384, 12288}},
	{"local-ffvf", {"Very Faint Object - Full frame", "./DSP/VeryFaintFullFrame.lod", 32768, 12288}},

	{"ffvb", {"Very Bright Object - Full frame", "/gem_sw/test/rcar/NIRS_DC/ioc/gnirsdc-mk-ioc/mk/cnf/sdsu/GNIRS/VeryBrightObjectFullFrame.lod", 1024, 2048}},
	{"ffb",  {"Bright Object - Full frame", "/gem_sw/test/rcar/NIRS_DC/ioc/gnirsdc-mk-ioc/mk/cnf/sdsu/GNIRS/BrightFullFrame.lod", 8192, 12288}},
	{"fff",  {"Faint Object - Full frame", "/gem_sw/test/rcar/NIRS_DC/ioc/gnirsdc-mk-ioc/mk/cnf/sdsu/GNIRS/FaintFullFrame.lod", 16384, 12288}},
	{"ffvf", {"Very Faint Object - Full frame", "/gem_sw/test/rcar/NIRS_DC/ioc/gnirsdc-mk-ioc/mk/cnf/sdsu/GNIRS/VeryFaintFullFrame.lod", 32768, 12288}},
	{"1", {"2R expose 2R - Full frame", "./DSP/Al-2R-e-2R.lod", 2048, 2048}},
	{"2", {"4R - Full frame", "./DSP/Al-4R.lod", 2048, 2048}},
	{"3", {"1R ex 1R ex 1R ex 1R ex - Full frame", "./DSP/Al-1R-e-1R-e-1R-e-1R-e.lod", 2048, 2048}},
	{"4", {"4R wait for clock - Full frame", "./DSP/Al-4R-wait.lod", 2048, 2048}},
	{"5", {"4R with reset after each - Full frame", "./DSP/Al-4R-reset.lod", 2048, 2048}},
	{"6", {"unrolled readout - Full frame", "./DSP/Al-4R-unrolled.lod", 2048, 2048}},
	{"7", {"init array between readout - Full frame", "./DSP/Al-4R-init.lod", 2048, 2048}},
	{"8", {"synthetic image - Full frame", "./DSP/synthetic.lod", 2048, 2048}}
};

using namespace arc::device;
using namespace arc::deinterlace;

class ExpIFace : public CExpIFace
{
public:
	ExpIFace(bool debugging) : dbg(debugging) {}

	void GeneralMessage( std::string& msg ) {
		if (dbg)
			std::cout << msg << '\n';
	}

	virtual void ExposeCallback( float fElapsedTime )
	{
		if (dbg) {
			std::cout << "Exposing. Elapsed time: " << ((fElapsedTime > 0.000001) ? fElapsedTime : 0.0) << '\n';
		}
	}

	virtual void ReadCallback( int dPixelCount )
	{
		if (dbg)
			std::cout << "Pixel Count: " << dPixelCount << '\n';
	}
private:
	bool dbg;
};

void print_help()
{
	std::cerr << "ARC Detector Testing Program\n\n"
		  << "   testing [-h] [-d] [-r] [-e seconds] <mode>\n\n"
		  << " -h     shows this help page\n"
		  << " -d     increased debugging output\n"
		  << " -r     resets the controller as part of the setup\n"
		  << " -e <s> expose por <s> seconds\n"
		  << " <mode> needs to be one of the following:\n\n"
	          << "     local-ffvb  Very Bright Object - Full frame\n"
	          << "     local-ffb   Bright Object - Full frame\n"
	          << "     local-fff   Faint Object - Full frame\n"
	          << "     local-ffvf  Very Faint Object - Full frame\n"
		  << "     ffvb        Very Bright Object (Full Frame)\n"
		  << "     ffb         Bright Object (Full Frame)\n"
		  << "     fff         Faint Object (Full Frame)\n"
		  << "     ffvf        Very Faint Object (Full Frame)\n"
	          << "     1           2R expose 2R - Full frame\n"
                  << "     2           4R - Full frame\n"
                  << "     3           1R ex 1R ex 1R ex 1R ex - Full frame\n"
                  << "     4           4R wait for clock - Full frame\n"
                  << "     5           4R with reset after each - Full frame\n"
                  << "     6           unrolled readout - Full frame\n"
                  << "     7           init array between readout - Full frame\n"
                  << "     8           synthetic - Full frame\n";

}
/*
 * custom_expose is a high-level exposure function tailored specifically for the timing files
 * written for GNIRS/ARC
 */

enum class ExposurePhase {
	FIRST_READOUT,
	EXPOSING,
	SECOND_READOUT
};

void custom_expose(CArcDevice* dev, float expTime, int dRows, int dCols, CExpIFace* exp_iface=nullptr)
{
	int msec = int( expTime * 1000 );
//	ExposurePhase status = ExposurePhase::FIRST_READOUT;

/*	if (dev->Command( TIM_ID, STP ) != DON) {
		throw std::runtime_error("Stop idle clock failed");
	}
*/
	// Setting the exposure time
	if (dev->Command( TIM_ID, SET, msec ) != DON) {
		throw std::runtime_error("Set exposure time failed");
	}

	// Start the exposure
	if (dev->Command( TIM_ID, SEX ) != DON) {
		throw std::runtime_error("Starting exposure failed");
	}

	const int totalCount = dRows * dCols;
std::cerr << "Rows Cols: " << dRows << "," << dCols << std::endl;
//	const int exposeTimeout = (int(expTime * 1000) / 25) + 20; // Exposure time + 0.5s, in 25 millisecond ticks
	int pixelCount = 0;
//	int timeoutCounter = 0;
//	float remainingTime = expTime;
	int exposingCount = 0;
//
//
//
//
//


	std::cerr << "Waiting for readout to begin";
	while ( !dev->IsReadout()) {
		std::cerr << ".";
		usleep( 25000 ); // Sleep for 25ms
	}
	std::cerr << std::endl;
	
       while ( pixelCount < totalCount) {
  				
		pixelCount = dev->GetPixelCount();


		if ((exp_iface != nullptr) && (pixelCount == totalCount / 2)) {
			std::cerr << "1.1	readingOut: " << dev->IsReadout() << " pixel count: " << pixelCount << "/" << totalCount << " exposing time: " << float(exposingCount * 25) / 1000 << std::endl;

			exp_iface->ExposeCallback(float(exposingCount * 25) / 1000);
			exposingCount++;
		}
		else if (exp_iface != nullptr) {
			std::cerr << "1.1	readingOut: " << dev->IsReadout() << " pixel count: " << pixelCount << "/" << totalCount << std::endl;
			exp_iface->ReadCallback(pixelCount);
		}

		usleep( 25000 ); // Sleep for 25ms
	}


	std::cerr << "Wait for readout to officially end" << std::endl;
	
	while ( dev->IsReadout()) {
		std::cerr << ".";
		usleep( 25000 ); // Sleep for 25ms
	}

	if (exp_iface != nullptr) {
		pixelCount = dev->GetPixelCount();
               	exp_iface->ReadCallback(pixelCount);
		std::cerr << "1.4 	pixelCount: " << pixelCount << std::endl;
	}

/*

       while ( pixelCount < totalCount / 2) {
  				
		std::cerr << "2.1	readingOut: " << dev->IsReadout() << std::endl;
		std::cerr << "2.2 	pixelCount: " << pixelCount << std::endl;
		std::cerr << "2.3 	totalCount: " << totalCount << std::endl;

		pixelCount = dev->GetPixelCount();


		if (exp_iface != nullptr) {
			exp_iface->ReadCallback(pixelCount);
		}

		usleep( 25000 ); // Sleep for 25ms
	}

	if (exp_iface != nullptr) {
                       exp_iface->ReadCallback(pixelCount);
        }
	std::cerr << "2.4 	pixelCount: " << pixelCount << std::endl;


*/

/*
       while ( pixelCount < totalCount ) {
               bool readingOut = dev->IsReadout();

		bool exposing = (status == ExposurePhase::FIRST_READOUT) && (pixelCount == (totalCount / 2));
		int lastPixelCount = pixelCount;
std::cerr << "1-Exp: " << exposing << "\n";
std::cerr << "1.1-readingOut: " << readingOut << std::endl;
		switch (status) {
			case ExposurePhase::FIRST_READOUT:
std::cerr << "2-First\n";
			case ExposurePhase::SECOND_READOUT:
std::cerr << "3-Second\n";
				pixelCount = dev->GetPixelCount();
				if (exposing) {
					if (!readingOut) {
						timeoutCounter = 0;
						status = ExposurePhase::EXPOSING;
						continue;
					}

					if (exp_iface != nullptr) {
std::cerr << "4-expose callback\n";
						exp_iface->ExposeCallback(float(exposingCount * 25) / 1000);
					}

					exposingCount++;
				}

				if (!exposing && (exp_iface != nullptr))
					exp_iface->ReadCallback(pixelCount);

				if (dev->ContainsError(pixelCount)) {
					dev->StopExposure();
					throw std::runtime_error("Failed to read pixel count");
				}


				if (!exposing) {
					timeoutCounter = (pixelCount == lastPixelCount) ? (timeoutCounter + 1) : 0;

					if (timeoutCounter >= 800) { // 20s * 40 slices
						dev->StopExposure();
						throw std::runtime_error("Read timeout");
					}
				}
				break;
			case ExposurePhase::EXPOSING:
std::cerr << "5-Exposing \n";
				if (readingOut) {
					if (exp_iface != nullptr) {
std::cerr << "6-expose callback\n";
						exp_iface->ExposeCallback(expTime);
}
					timeoutCounter = 0;
					status = ExposurePhase::SECOND_READOUT;
std::cerr << "6.1-status = SECOND_READOUT\n";
					continue;
				}
				if (remainingTime > 0.0) {
					int ret = dev->Command( TIM_ID, RET );

					if (ret != ROUT) {
						if (dev->ContainsError(ret) || dev->ContainsError(ret, 0, msec)) {
							dev->StopExposure();
							throw std::runtime_error("Failed to read elapsed time");
						}

						float elapsedTime = (float(ret) / 1000.0);
						remainingTime = expTime - elapsedTime;
						if (exp_iface != nullptr)
							exp_iface->ExposeCallback(elapsedTime);

					}
				}
std::cerr << "7-timeoutCounter: " << timeoutCounter << " timeout: " << exposeTimeout << std::endl;
				if ((++timeoutCounter) > exposeTimeout) {
					dev->StopExposure();
					throw std::runtime_error("Timeout while exposing");
				}
				break;

		}



		usleep( 25000 ); // Sleep for 25ms
	}
*/
	dev->StopExposure();
}

void parse_cmd(int argc, char **argv, Config& mode, bool& reset, bool& debug)
{
	bool found_mode = false;
	bool exp_set = false;
	float exp = 0.0;

	for (int argi = 0; argi < argc; ++argi) {
		std::string current(argv[argi]);

		if (current == "-h") {
			print_help();
			exit(0);
		}
		else if (current == "-r") {
			reset = true;
		}
		else if (current == "-d") {
			debug = true;
		}
		else if (current == "-e") {
			++argi;
			if (argi >= argc) {
				std::cerr << "Missing argument for -e\n";
				exit(1);
			}

			exp_set = true;
			exp = std::stof(argv[argi]);
		}
		else {
			if (setups.count(current) == 0) {
				std::cerr << "Unknown mode " << current << ". Pass -h if you need a list\n";
				exit(1);
			}

			mode = setups.at(current);
			if (exp_set) {
				mode.exposure = exp;
			}
			return;
		}
	}

	if (!found_mode) {
		std::cerr << "Need an operating mode. Pass -h if you need a list\n";
		exit(1);
	}
}

int main(int argc, char **argv) {
	bool reset = false;
	bool debug = false;
	Config mode{"", "", 0, 0, 0.0};

	parse_cmd(argc-1, &argv[1], mode, reset, debug);

	Controller cont(mode.nrows, mode.ncols);

	if (debug) {
		std::cout << "Testing for mode: " << mode.label << '\n';
	}
	std::cout << "Exposing for " << mode.exposure << " seconds\n";

	cont.connect_device();

	std::cout << "List of devices:\n";
	for (auto st: cont.device_list()) {
		std::cout << "  " << st << '\n';
	}

	std::cout << "TDL testing: " << cont.tdl_testing(123) << '\n';




	std::cout << "Performing reset\n";
	cont.getDev()->Reset();




//	unsigned int bias_mode = BVM;
//	std::cout << "Setting bias level 0x" << std::hex << bias_mode << std::dec <<'\n';
//	cont.bias_testing(bias_mode);



	if (debug)
		std::cout << "Setting up with file" << mode.lod_file << '\n';
	cont.setup_controller(mode.lod_file, true, reset); // Power on
	if (!reset)
		cont.set_size(mode.nrows, mode.ncols);
	// cont.set_synthetic(false);
	if (debug)
		std::cout << "Exposing\n";

	ExpIFace callbacks(debug);
//	cont.start_logging();
//	cont.expose(mode.exposure, true, &callbacks);
	custom_expose(cont.getDev(), mode.exposure, mode.nrows, mode.ncols, &callbacks);
//	cont.stop_logging(std::cout);

	cont.save_to("test_file.fits");

//	custom_expose(cont.getDev(), mode.exposure, mode.nrows, mode.ncols, &callbacks);

//	cont.save_to("test_file2.fits");

	return 0;
}
